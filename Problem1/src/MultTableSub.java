
public class MultTableSub
{

  final int PERLINE = 4;
  int mult, count = 0;
  int lowPerimeter = 1;
  int value = lowPerimeter;
  final int MAXPERIMETER = 12;

  public MultTableSub()
  {

  }

  public int tablePrint()
  {
    for (mult = value; mult <= MAXPERIMETER; mult += lowPerimeter)
    {
      System.out.print((value * value) + "\t");
      value++;
      count++;
      if (count % PERLINE == 0)
        System.out.println();
    }
    return MAXPERIMETER;
  }
}
