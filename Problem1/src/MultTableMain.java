
//********************************************************************
//File:         MaltTableMain.java       
//Author:       Liam Quinn
//Date:         October 24th 2017
//Course:       CPS100
//
//Problem Statement:
// Write a program that produces a multiplication table, showing
// the results of multiplying the integers 1 through 12 by themselves
// 
//Inputs: None
//Outputs:  A multiplication table of all numbers between 1 and 12 
//          times by themselves.
// 
//********************************************************************

public class MultTableMain
{

  public static void main(String[] args)
  {
    MultTableSub link = new MultTableSub();
    int lowPerimeter = 1;
    final int MAXPERIMETER = 12;

    System.out.println("The multiples of each number times by themselves "
                       + "between " + lowPerimeter + " and " + MAXPERIMETER
                       + " are: ");

    link.tablePrint();

  }

}
