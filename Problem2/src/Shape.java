import java.util.Scanner;


public class Shape
{
  final int MAXROWS = 10;
  final int MINROWS = 1;
  final int D_MAXROWS = 5;
  Scanner scan = new Scanner(System.in);


  public Shape()
  {

  }

  public void shapeA()
  {
    // A
    System.out.println("How many rows? (Integer)");

    System.out.println("a)");
    System.out.println();
    for (int aRow = scan.nextInt(); aRow >= MINROWS; aRow--)
    {
      for (int aStar = 1; aStar <= aRow; aStar++)
        System.out.print("*");

      System.out.println();
    }
    System.gc();
  }

  public void shapeB()
  {
    // B
    System.out.println("How many rows? (Integer)");
    int bInt = scan.nextInt();

    System.out.println("b)");
    System.out.println();
    for (int bRow = bInt; bRow >= MINROWS; bRow--)
    {
      for (int bSpace = 0; bSpace <= bRow - 1; bSpace++)
      {
        System.out.print(" ");

      }
      for (int bStar = bInt; bStar >= bRow; bStar--)
      {
        System.out.print("*");

      }
      System.out.println();
    }
    System.gc();
  }

  public void shapeC()
  {
    // C
    System.out.println("How many rows? (Integer)");
    int cInt = scan.nextInt();

    System.out.println("c)");
    System.out.println();
    for (int cRow = cInt; cRow >= MINROWS; cRow--)
    {
      for (int cSpace = cInt; cSpace >= cRow - 1; cSpace--)
      {
        System.out.print(" ");

      }
      for (int cStar = 1; cStar <= cRow; cStar++)
      {
        System.out.print("*");

      }
      System.out.println();
    }
    System.gc();
  }

  public void shapeD()
  {
    // D
    System.out.println("How many rows? (Even Integer)");
    int dInt = scan.nextInt();
    int ddInt = dInt / 2;

    System.out.println("d)");
    System.out.println();
    for (int dRow = 1, dChar = 1; dRow <= ddInt; dRow++, dChar = dChar + 2)
    {
      // Increasing
      for (int dSpace = ddInt - 1; dSpace >= dRow - 1; dSpace--)
      {
        System.out.print(" ");

      }
      for (int dStar = 1; dStar <= dChar; dStar = dStar + 1)
      {
        System.out.print("*");
      }
      System.out.println();
    }

    for (int dRow = 1, dChar = 1; dRow <= dInt; dRow++, dChar = dChar + 2)
    {
      // Decreasing
      for (int dSpace = 1; dSpace <= dRow; dSpace++)
      {
        System.out.print(" ");

      }
      for (int dStar = dInt - 1; dStar >= dChar; dStar = dStar - 1)
      {
        System.out.print("*");
      }
      System.out.println();
    }
    System.gc();
  }
}
