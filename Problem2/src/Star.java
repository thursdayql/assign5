//********************************************************************
//File:         Star.java       
//Author:       Liam Quinn
//Date:         October 25th 2017
//Course:       CPS100
//
//Problem Statement:
// Create modified versions of the Stars program to print the following
// patterns. Create a separate program to produce each pattern.
//
// 
//Inputs: None
//Outputs:  A multiplication table of all numbers between 1 and 12 
//          times by themselves.
// 
//********************************************************************

public class Star
{

  public static void main(String[] args)
  {
    Shape link = new Shape();
    System.gc();

    // Triangle A
    link.shapeA();

    System.out.println();
    System.out.println();


    // Triangle B
    link.shapeB();

    System.out.println();
    System.out.println();


    // Triangle C
    link.shapeC();

    System.out.println();
    System.out.println();


    // Triangle D
    link.shapeD();

    System.out.println();
    System.out.println();
  }

}
